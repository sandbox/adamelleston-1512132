<?php

/**
 * Simple custom text box.
 */
class flickr_pull_boxes_simple extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    return array(
      'source_id' => '',
      'source_type' => 'set',
      'per_page' => '6',
    );
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {    
    $form = array();
    $form['source_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Source ID'),
      '#default_value' => $this->options['source_id'],
      '#description' => t('The ID used by flickr for the photoset, gallery or group.'),
    );
    $form['source_type'] = array(
      '#type' => 'select',
      '#title' => t('Feed type'),
      '#default_value' => $this->options['source_type'],
      '#options' => array(
        'set' => 'Photoset',
        'gallery' => 'Photo gallery',
        'group' => 'Group',
      ),
      '#description' => t('Is this a photoset, gallery or group.'),
    );
    // Go from 0 so we can get keys and values to match
    $numOptions = range(0, 20);
    // Then remove 0 so users can't select it
    unset($numOptions[0]);
    $form['per_page'] = array(
      '#type' => 'select',
      '#title' => t('Photo limit'),
      '#default_value' => $this->options['per_page'],
      '#options' => $numOptions,
      '#description' => t('How many photos to show in the block.'),
    );
    return $form;  
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    $content = flickr_pull_render($this->options['source_type'], array('per_page' => $this->options['per_page']), $this->options['source_id']);    
    $title = isset($this->title) ? $this->title : NULL;
    return array(
      'delta' => $this->delta, // Crucial.
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}
