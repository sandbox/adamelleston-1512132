<ul class="flickr-pull flickr-pull-gallery">
<?php foreach ($photos as $photo): ?>
  <li class="item">
    <?php echo l(theme('image', $photo), $photo['url'], array('html' => TRUE, 'attributes' => array('rel' => 'lightbox['.$photo['flickr_type'].']['.$photo['title'].' - '.$photo['alt'].']'))); ?>
  </li>
<?php endforeach; ?>
</ul>
<div class="flickr-pull-clear"></div>